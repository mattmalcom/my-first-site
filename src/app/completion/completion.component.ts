import { Component, OnInit } from '@angular/core';
import {DataService} from '../data.service';
import {data_Slvl} from '../data_Slvl';
import {data_Plvl} from '../data_Plvl';

@Component({
  selector: 'app-completion',
  templateUrl: './completion.component.html',
  styleUrls: ['./completion.component.css']
})
export class CompletionComponent implements OnInit {

Slevels: data_Slvl[];
Plevels : data_Plvl[];

getLevels(){

  this.dataService.getSLevels()
  .subscribe(levels => this.Slevels = levels);

  this.dataService.getPLevels()
  .subscribe(levels => this.Plevels = levels);
}

  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.getLevels();
  }

}
