import { Component, OnInit,Input } from '@angular/core';
import{data_Slvl} from '../data_Slvl';
import {DataService} from '../data.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { stringify } from 'querystring';

@Component({
  selector: 'app-slevel-detail',
  templateUrl: './slevel-detail.component.html',
  styleUrls: ['./slevel-detail.component.css']
})
export class SlevelDetailComponent implements OnInit {

    @Input() theLevel: data_Slvl;  
    title:string;

  constructor(
    private route :ActivatedRoute,
    private dataService: DataService,
    private location: Location) { 
    
  }

  ngOnInit() {
    this.getSlvl();
  }

  getSlvl(): void{
     const id = +this.route.snapshot.paramMap.get('id');
    this.dataService.getSlvl(id)
      .subscribe(Level => this.theLevel = Level);


  }


}
