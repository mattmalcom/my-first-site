import { Component, OnInit } from '@angular/core';
import {DataService} from '../data.service';
import {data_char} from '../data_char';

@Component({
  selector: 'app-character',
  templateUrl: './character.component.html',
  styleUrls: ['./character.component.css']
})
export class CharacterComponent implements OnInit {

  chars: data_char[];
  game: string;
  isGood: boolean;
 

  handleFlags(button: string): string{

    if(button == null){
      return this.game;
    }
    if(button == this.game){
      this.game = '';
      return '';
    }
    this.game = button;
   return button;
    
   } 

   handleGood(newGood: boolean){
    if(newGood == null){
      return this.isGood;
    }
    if(newGood == this.isGood){
      this.isGood = null;
      return null;
    }
    else{
      this.isGood = newGood;
      return newGood;
    }
    
   }
  
    getChars(game:string,isGood: boolean): void{
      
      game = this.handleFlags(game);
      isGood = this.handleGood(isGood);
  
      this.dataService.getChars(game,isGood)
      .subscribe(Chars => this.chars = Chars);
    }

  constructor(private dataService: DataService) { }

  ngOnInit() {
  this.getChars('',null);
  }

}
