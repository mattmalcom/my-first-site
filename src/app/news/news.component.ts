import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import {data_news} from '../data_news';



@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit {

  game: string;
  articles: data_news[];
  constructor(private dataService: DataService)  { }

 handleFlags(button: string): string{

  if(button == this.game){
    console.log('well');
    this.game = '';
    return '';
  }
  this.game = button;
 return button;
  
 } 

  getArticles(game:string): void{
    
    game = this.handleFlags(game);

    this.dataService.getArticles(game)
    .subscribe(Articles => this.articles = Articles);
  }

  ngOnInit() {
    this.getArticles(null);
    this.game = '';
    
    
  }

}
