import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http'
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService }  from './in-memory-data.service';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { NewsComponent } from './news/news.component';
import { CompletionComponent } from './completion/completion.component';
import { SpeedrunComponent } from './speedrun/speedrun.component';
import { CharacterComponent } from './character/character.component';
import { SlevelDetailComponent } from './slevel-detail/slevel-detail.component';
import { PlevelDetailComponent } from './plevel-detail/plevel-detail.component';



@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    NewsComponent,
    CompletionComponent,
    SpeedrunComponent,
    CharacterComponent,
    SlevelDetailComponent,
    PlevelDetailComponent,
   
  ],
  imports: [
    HttpClientModule,
    HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService, {dataEncapsulation: false}
    ),
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
