import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SlevelDetailComponent } from './slevel-detail.component';

describe('SlevelDetailComponent', () => {
  let component: SlevelDetailComponent;
  let fixture: ComponentFixture<SlevelDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SlevelDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SlevelDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
