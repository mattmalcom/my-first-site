import {data_skel,mission,enemy} from './data_skel';


export class data_Slvl extends data_skel{

    enemies : enemy[]
    mapUrl: string;
    missions : mission[];
    id: number; 


    constructor(Ctitle: string,Cgame: string,CmapUrl: string,Cid: number,ens: enemy[],missions: mission[]){
        super(Ctitle,Cgame,`/Slevel-detail/${Cid}`);
        this.elementType = 'level';
        this.mapUrl = CmapUrl;
        this.missions = missions;
        this.id = Cid;
        this.enemies = ens;

    }


}