import { Component, OnInit } from '@angular/core';
import { DataService} from '../data.service';
import { data_speed} from '../data_speed';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-speedrun',
  templateUrl: './speedrun.component.html',
  styleUrls: ['./speedrun.component.css']
})
export class SpeedrunComponent implements OnInit {

runs: data_speed[];
safeUrl : SafeResourceUrl;
id: string;

getRuns(): void{

  this.dataService.getRuns()
  .subscribe(Runs => this.runs = Runs);

}

setRunId(runId:string):void{
  this.id = runId;
  this.safeUrl = this.sanitizer.bypassSecurityTrustResourceUrl(`https://www.youtube.com/embed/${runId}`);
}

  constructor(private dataService: DataService,private sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.getRuns();

  }

}
