export class data_skel{

    elementType: string;
    title: string;
    game: string;
    searchUrl: string;
    

constructor(Ctitle: string,Cgame: string,searchUrl:string){
this.title = Ctitle;
this.game = Cgame;
this.searchUrl = searchUrl;
 
}

}

export class mission{
    name:string;
    desc:string;
    iconUrl:string;
    asign: string;

    constructor(name:string,desc:string,iconUrl:string,Casign: string){
        this.name = name;
        this.desc = desc;
        this.iconUrl = iconUrl;
        this.asign = Casign;
    }

}

export class enemy{
    name:string;
    desc:string;
    imgUrl: string;


    constructor(name:string,desc:string,iconUrl:string){
        this.name = name;
        this.desc = desc;
        this.imgUrl = iconUrl;
    }

}