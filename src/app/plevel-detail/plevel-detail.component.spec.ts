import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlevelDetailComponent } from './plevel-detail.component';

describe('PlevelDetailComponent', () => {
  let component: PlevelDetailComponent;
  let fixture: ComponentFixture<PlevelDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlevelDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlevelDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
