import { Component, OnInit,Input } from '@angular/core';
import{ data_Plvl} from '../data_Plvl';
import {DataService} from '../data.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { stringify } from 'querystring';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap'; 

@Component({
  selector: 'app-plevel-detail',
  templateUrl: './plevel-detail.component.html',
  styleUrls: ['./plevel-detail.component.css']
})
export class PlevelDetailComponent implements OnInit {

  @Input() theLevel: data_Plvl;  
    title:string;
    closeResult: string;
    urls: string[];
    index: number;

  constructor(
    private route :ActivatedRoute,
    private dataService: DataService,
    private location: Location,
    private modalService: NgbModal
    ) { 
    
  }

  inc(){
    
    (this.index == (this.urls.length - 1))
    ? this.index = 0
    : this.index++;
    
  }

  dec(){
    (this.index == 0)
    ? this.index = (this.urls.length - 1)
    : this.index--;

  }

  open(content,urls: string[]) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'})
    this.urls = urls;
    this.index = 0;
  }

  ngOnInit() {
    this.getPlvl();
  }

  getPlvl(): void{
     const id = +this.route.snapshot.paramMap.get('id');
    this.dataService.getPlvl(id)
      .subscribe(hero => this.theLevel = hero);
  }

  
}
