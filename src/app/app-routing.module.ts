import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {NewsComponent} from './news/news.component';
import { CompletionComponent } from './completion/completion.component';
import { SpeedrunComponent } from './speedrun/speedrun.component';
import { CharacterComponent } from './character/character.component';
import { SlevelDetailComponent } from './slevel-detail/slevel-detail.component';
import { PlevelDetailComponent } from './plevel-detail/plevel-detail.component';



const routes: Routes = [
  {path: 'news', component: NewsComponent},
  {path: 'completion', component: CompletionComponent},
  {path: 'speed', component: SpeedrunComponent},
  {path: 'character', component: CharacterComponent},
  {path: 'Slevel-detail/:id', component: SlevelDetailComponent},
  {path: 'Plevel-detail/:id', component:PlevelDetailComponent},
  {path: '', redirectTo: 'path', pathMatch : 'full'}
  
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
