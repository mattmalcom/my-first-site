import {data_skel} from './data_skel';

export class data_Plvl extends data_skel{

    id:number;
    vaults : string[][];
    desc: string;
    charUrl: string;
    powUrl: string
    powDesc: string;
    powTitle: string;

    constructor(Ctitle: string,id: number,vaultUrls :string[][],desc: string,charUrl: string,powUrl: string,powDesc: string,powTitle: string){
        super(Ctitle,'pn',`/Plevel-detail/${id}`);
        this.elementType = 'level';
        this.id = id; 
        this.vaults = vaultUrls;
        this.desc = desc;
        this.charUrl = charUrl;
        this.powUrl = powUrl;
        this.powDesc = powDesc;
        this.powTitle = powTitle;

    }



}