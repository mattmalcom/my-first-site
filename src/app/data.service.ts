import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {data_skel} from './data_skel';
import {data_news} from './data_news';
import { Observable, of } from 'rxjs';
import {catchError, map,tap} from 'rxjs/operators';
import { stringify } from '@angular/compiler/src/util';
import { data_char } from './data_char';
import {data_Slvl} from './data_Slvl';
import {data_speed} from './data_speed';
import {data_Plvl} from './data_Plvl';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  goodString: string;

  constructor(
    private http: HttpClient
  ) { }

    private dataUrl = 'api/elements';

  getData(): Observable<data_skel[]>{
    return this.http.get<data_skel[]>(this.dataUrl);

  }

  searchData(term: string): Observable<data_skel[]>{
    if (!term.trim()) {
      return of([]);
    }
    return this.http.get<data_skel[]>(`${this.dataUrl}/?title=${term}`).pipe(
      tap(_ => this.log(`found items`)),
      catchError(this.handleError<data_skel[]>('searchHeroes', []))
    );

  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
  
      console.error(error);
      this.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }

  private log(message: string) {
    console.log(message);
  }


  getArticles(game: string): Observable<data_news[]>{
   
    if(game == null){
     game = '';
    }
    else{
      game = `&game=${game}`;
    }
     
    return this.http.get<data_news[]>(`${this.dataUrl}/?elementType=news${game}`).pipe(
      tap(_ => this.log(`found articles`)),
      catchError(this.handleError<data_news[]>('searchHeroes', []))
    );
    
  }
  
  getChars(game: string,isGood: boolean): Observable<data_char[]>{
    
    if(isGood == null){
      this.goodString = '';
    }
    else if(isGood){
      this.goodString = '&isGood=true';
    }
    else{
      this.goodString = '&isGood=false';

    }

    if(game == null){
     game = '';
    }
    else{
      game = `&game=${game}`;
    }

    
     
    return this.http.get<data_char[]>(`${this.dataUrl}/?elementType=char${game}${this.goodString}`).pipe(
      tap(_ => this.log(`found characters `)),
      catchError(this.handleError<data_char[]>('searchHeroes', []))
    );
    
  }

  getSLevels(): Observable<data_Slvl[]>{
    
    return this.http.get<data_Slvl[]>(`${this.dataUrl}/?elementType=level&game=sly`).pipe(
      tap(_ => this.log(`found levels `)),
      catchError(this.handleError<data_Slvl[]>('searchHeroes', []))
    );

  }

  getPLevels(): Observable<data_Plvl[]>{
    
    return this.http.get<data_Plvl[]>(`${this.dataUrl}/?elementType=level&game=pn`).pipe(
      tap(_ => this.log(`found levels`)),
      catchError(this.handleError<data_Plvl[]>('searchHeroes', []))
    );

  }

  getSlvl(id: number): Observable<data_Slvl>{

    const url = `${this.dataUrl}/${id}`;
    return this.http.get<data_Slvl>(url).pipe(
      tap(_ => this.log(`found level`)),
      catchError(this.handleError<data_Slvl>(`getHero id=${id}`))
    );

  }

  getPlvl(id: number): Observable<data_Plvl>{

    const url = `${this.dataUrl}/${id}`;
    return this.http.get<data_Plvl>(url).pipe(
      tap(_ => this.log(`found level`)),
      catchError(this.handleError<data_Plvl>(`getHero id=${id}`))
    );

  }

  getRuns(): Observable<data_speed[]> {
    return this.http.get<data_speed[]>(`${this.dataUrl}/?elementType=speed`).pipe(
      tap(_ => this.log(`fetched runs`)),
      catchError(this.handleError<data_speed[]>(`getHero id=`))
    );

  }

}
