 import {data_skel} from './data_skel';


export class data_news extends data_skel{

    description: string;

constructor(Ctitle: string,Cgame: string,Cdesc: string){
    super(Ctitle,Cgame,'/news');
    this.elementType = "news"
    this.description = Cdesc;

}

}