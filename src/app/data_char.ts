import {data_skel} from './data_skel';


export class data_char extends data_skel{
  
    imageUrl: string; 
    isGood: boolean;
    desc: string;
    
    constructor(Ctitle: string,Cgame: string,CUrl: string,CisGood: boolean,desc: string){
        
        super(Ctitle,Cgame,'/character');
        this.elementType = "char";
        this.imageUrl = CUrl;
        this.isGood = CisGood;
        this.desc = desc;
        
    }        


}