import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { data_skel,mission,enemy } from './data_skel';
import {data_news} from './data_news';
import {data_char} from './data_char';
import {data_Slvl}  from './data_Slvl';
import {data_speed} from './data_speed';
import { data_Plvl } from './data_Plvl';

@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService {

  //ok so this is basically a fake DB until i learn more about
  // the backend and so i am going to extend the d_s class and
  // make all of the other types of data out of it and 
  // then instantiate the object up here then acctually insert
  // them into the DB in the method underneath it

  slycon: string = '../../assets/images/icons/sly_icon.png';
  murcon: string = '../../assets/images/icons/murray_icon.png';
  bencon: string = '../../assets/images/icons/bentley_icon.png';
  assetFolder: string = '../../assets/images/';

//==============================news articles
james_in_P2 = new data_news("James willems confirmed for psychonauts 2","pn","after voiceing his desire to be a part of a potential sequel to psychonauts Tim shcaffer has accepted James Willems of Funhaus to play the part of \"toilet number 4\" in the game, slated for a 2020 release");

sly_in_egypt = new data_news('sly cooper is still stuck in egypt',"sly", "sly is still in egypt and shit and now it's been six years and no one has touched the property of sly cooper so i makes sense to assume he is offcially dead"); 

new_rDate = new data_news("Psychonauts 2 pushed back to 2020 release date","pn","after the game's initial announcement in 2015 and the original planned release window of late 2017 Tim Schaffer and his team have decided they will need until 2020 to make sure psychonauts 2 lives up to its expectations meaning it will be another 5 year dev cycle like its predecessor");

//==============================news articles

//=================================characters
raz = new data_char("Razputin aquato","pn",'../../assets/images/heroes/raz.png',true,"Born into a family of acrobats, Raz found himself to be an incredibly capable psychic. His father, having always despised psychics for the curse they layed on their family forbids Raz from practicing his abilities, this only drives him to run away from his family circus and to the whispering rock psychic summer camp where he hopes to hone his skill and become a psychonauts. However upon arrival strange things begin happening in at the camp and within his own mind and its up to him to discover whats causing the disturbances and put a stop to it"); 

sly = new data_char("Sly Cooper","sly",'../../assets/images/heroes/Sly_Cooper.png',true,"Born into a long line of master thieves Sly was set to inherit the coveted book of the cooper clan when Group of five criminals broke into his home murdered his family and stole the thieveous racoonus. Afterward sly found himself in an orphange where he met bentley and Murray who went on to form the cooper gang and steal back the book. from there he continued to perform masterful thieving operations always stealing from master criminals as id the tradition of the cooper family "); 

murray = new data_char("Murray the hippo", "sly", '../../assets/images/heroes/Murray.png',true,"Not much is Known about Murray's past aside from the fact that he is an orphan much like Sly, taking on the role of the muscle in the cooper gang, Murray has gotten the gang out of multiple scrapes through his sheer strength. after the break up of the cooper gang Murray went on to compete in race car derby and when bentley called upson sly and murray to discover what was occouring in the past of the cooper family he resumed his role in the gang");

clockwerk = new data_char("Clockwerk the owl", 'sly','../../assets/images/enemies/clockwerk.png',false ,"Born a normal owl clockwork was consumed with jealousy for Sly's family and so in replaced his organic body with mechanical parts in order to attain immortality and forever plauge the Cooper clan. He was the organizing force behind the fiendish five and sought to Kill sly's father personaly while still leaving sly alive as a way to prove that without the family book coopers would never become great theives.");

gman = new data_char("G men from milk man conspiracy",'pn',`${this.assetFolder}enemies/gman.png`,false,"These poorly disgused goverment agents run rampant within boyd Coopers mind and seem to manifiestations of Boyd's deep seeded distrust of authority and larger entities like the government. these shadowy figures are hellbent on the discovery of the milkman and turn out to be working in tandem with Boyd's censors");

boyd = new data_char("Boyd Cooper aka \"The Milkman\"","pn",`${this.assetFolder}heroes/Boyd.png`,true,"Previously a security gaurd, Boyd is a deranged conspiracy theorist now tasked with gatekeeping the insane asylum Raz has found his fellow campers brains at. Boyd became deranged after being fired from his security job and attempting to burn down his old work place with molatov cocktails and from there coach olyander planted the idea of the milkman in his head.");
//=================================characters

//=====================================sly level paris
parMissions = [new mission("Satellite Sabotage","Sly must orient 3 sattelites so he and the gang can spy on Dimitri",this.slycon,'sly'),
              new mission('breaking and entering', "sly must work together with murray to infiltrate dimitri's literal underground operations and take recon photos",this.slycon,'sly'),
              new mission('bug Dmitri\'s office','sly must sneak a forged paiting outfitted with a microphone into Dmitri\'s office',this.slycon,'sly'),
              new mission('water pump destruction','after sly tailed dmitri murray must disable the aqua pump room of the sake of the heist',this.murcon,'murray'),
              new mission('disco demolitions', 'in order to knock the peackcock sign lose so that is will smash into the roof of dimtri\'s operation bentley must enter the night club and dismantle the supports of the giant mirror ball with explosives',this.bencon,'bentley')
            ];

parEns = [new enemy('rat gaurd', 'a small rooftop gaurd incapable of calling for help',`${this.assetFolder}enemies/rat_gaurd.jpg`),
          new enemy('warthog gaurd','a large flashlight gaurd armed with a pistol incapable of going on rooftops',`${this.assetFolder}enemies/warthog_gaurd.png`),
         new enemy('frog gaurd','a small rooftop gaurd capable of calling for help',`${this.assetFolder}enemies/frog_gaurd.jpg`)];

slvl_paris = new data_Slvl("Paris from Sly 2",'sly',`${this.assetFolder}maps/paris_map.png`,1,this.parEns,this.parMissions);

//=====================================sly level paris

//=====================================sly level india
inMissions = [
    new mission("Lower the drawbridge","in order for bentley and murray to do anything in the palace sly must break in and open the drawbridge",this.slycon,'sly'),
    new mission("Battle the chopper", 'part of the final heist requires clear skys and bentley needs the gaurd helicopter taken down',this.murcon,'murray'),
    new mission("bomb the bridge","in order to cut off reinforcements from the rest of the palace during the heist bentley must bestroy the bridge to the geust house",this.bencon,'bentley')

];

inEns = [

  new enemy("baboon gaurd", "a small rooftop gaurd, incapable of calling for help and launched very far by attacks",`${this.assetFolder}enemies/baboon_gaurd.jpg`),
  new enemy("ibex gaurd", "simple rooftop enemy capable of calling for backup",`${this.assetFolder}enemies/Ibex_gaurd.jpg`),
  new enemy("Rhino gaurd","large flashlight gaurd armes with throwable swords",`${this.assetFolder}enemies/Rhino_gaurd.jpg`)  

];

slvl_india = new data_Slvl("india from Sly 2",'sly',`${this.assetFolder}maps/india_map.png`,2,this.inEns,this.inMissions);

//=====================================sly level india

//===========================pn level mmc

mmc_vaults = [["../../assets/images/vaults/mmc1_1.jpg","../../assets/images/vaults/mmc1_2.png","../../assets/images/vaults/mmc1_3.png","../../assets/images/vaults/mmc1_4.png","../../assets/images/vaults/mmc1_5.png","../../assets/images/vaults/mmc1_6.png","../../assets/images/vaults/mmc1_7.png","../../assets/images/vaults/mmc1_8.png"],
["../../assets/images/vaults/mmc2_1.jpg","../../assets/images/vaults/mmc2_2.png","../../assets/images/vaults/mmc2_3.png","../../assets/images/vaults/mmc2_4.png","../../assets/images/vaults/mmc2_5.png","../../assets/images/vaults/mmc2_6.png"],];

plvl_mmc = new data_Plvl('The Milk man conspiracy',
3,
this.mmc_vaults,
'Boyd cooper is a deranged conspiracy theorist with the hyper violent alter ego Known as \"The Milkman\" .Boyd\'s mind has become warped and twisted as a result of his conspiracy theories he believes and all around this warped world are watching eyes and cameras illustrating boyd\'s constatnt paranoia. Raz must investigate and find the Milk Man within Boyd\'s mind so he will don the persona in the real world and open the gate to the asylum for Raz',
`${this.assetFolder}heroes/Boyd.png`,
`${this.assetFolder}icons/clair_icon.png`,
'This power given to Raz by boyd allows for Raz to see the world as other beings do either by being close and seeing directly through their eyes or holding onto an item related to them and connecting to them through that',
'Clairvoyance'
);


//===========================pn level mmc



//======================================speed runs

DF_spec_run = new data_speed("SMK speed run done in front of the Double Fine team",'pn','lsDc1YVxHA0');
GDQ_sly1_2019 = new data_speed('Zenthrow\'s sly 1 run done at GDQ 2019','sly','UsjIzPYLtqg');
Sly2_WR = new data_speed("current world record for sly 2 speed run in english",'sly','b9HkG_V9HzI');

//======================================speed runs


createDb(){

    const elements =  [
      this.slvl_paris,
      this.new_rDate,
      this.clockwerk,
      this.raz,
      this.sly,
      this.murray,
      this.james_in_P2,
      this.sly_in_egypt,
      this.gman,
      this.slvl_india,
      this.DF_spec_run,
      this.GDQ_sly1_2019,
      this.Sly2_WR,
      this.plvl_mmc,
      this.boyd

    ];

return {elements};

  }

  constructor() { }
}
